package jruchel.labyrinth;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Scanner;

import jruchel.labyrinth.maze.Maze;
import jruchel.labyrinth.maze.MazeSolver;
import jruchel.labyrinth.maze.Tile;
import jruchel.labyrinth.maze.TileDisplay;

public class Main {

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void main(String[] args) {
        Maze maze = new Maze(Maze.Difficulty.EASY);
        maze.setTilesDisplay(new TileDisplay() {
            @Override
            public void displayTop(Tile.Wall top, int i) {
                System.out.println(top.isOpen() ? "-" : " ");
            }

            @Override
            public void displayBottom(Tile.Wall bot, int i) {
                System.out.println(bot.isOpen() ? "-" : " ");
            }

            @Override
            public void displayLeft(Tile.Wall left, int i) {
                System.out.print(left.isOpen() ? "|" : " ");
            }

            @Override
            public void displayRight(Tile.Wall right, int i) {
                System.out.println(right.isOpen() ? "|" : " ");
            }

            @Override
            public void displayContent(String content, int i) {
                if (maze.getTiles().get(i).getContent().equals("X")) {
                    System.out.print("X");
                } else {
                    System.out.print(" ");
                }

            }
        });
        maze.display();

        MazeSolver solver = new MazeSolver(maze);
        solver.run();

        Scanner input = new Scanner(System.in);
        String choice;
        while (true) {
            choice = input.nextLine();
            switch (choice) {
                case "w":
                    maze.movePlayer(Tile.Direction.NORTH);
                    break;
                case "a":
                    maze.movePlayer(Tile.Direction.WEST);
                    break;
                case "s":
                    maze.movePlayer(Tile.Direction.SOUTH);
                    break;
                case "d":
                    maze.movePlayer(Tile.Direction.WEST);
                    break;
            }
            maze.display();
        }

    }



}
