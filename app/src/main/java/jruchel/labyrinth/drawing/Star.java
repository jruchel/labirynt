package jruchel.labyrinth.drawing;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Star implements Drawable {

    private int points;
    private int size;
    private Point point;
    private int color;

    public Star(int size, Point point, int points, int color) {
        this.size = size;
        this.point = point;
        this.points = points;
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public Star(int size, Point point, int points) {
        this(size, point, points, Color.WHITE);
    }

    @Override
    public void draw(Canvas canvas, Paint drawPaint) {
        float angle = 360 / points;

        for (int i = 0; i < points; i++) {
            Point a = getPointByAngle(angle * ((i + 2) % points));
            Point b = getPointByAngle(angle * (i));
            Line line = new Line(a, b);
            line.draw(canvas, drawPaint);
            a = point;
            line = new Line(a, b);
            line.draw(canvas, drawPaint);
        }
        canvas.drawCircle((float) point.x, (float) point.y, size / 5, drawPaint);
    }

    private Point getPointByAngle(float angle) {
        angle -= 17.5;
        angle = (float) Math.toRadians(angle);
        double x = point.x + size * Math.cos(angle);
        double y = point.y + size * Math.sin(angle);

        return new Point(x, y);
    }
}

//Take a point, on a circle size distance away from the Point
//Draw