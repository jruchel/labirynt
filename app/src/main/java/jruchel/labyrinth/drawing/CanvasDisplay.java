package jruchel.labyrinth.drawing;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.List;


public class CanvasDisplay extends View {

    private List<Drawable> drawables;
    private Paint drawPaint;
    private Canvas canvas;


    public CanvasDisplay(Context context) {
        super(context);
        setupPaint();
        setupDrawables();

    }

    public CanvasDisplay(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupPaint();
        setupDrawables();
    }


    private void setupDrawables() {
        this.drawables = new ArrayList<>();
    }

    public void addLine(Line line) {
        drawables.add(line);
    }

    public void update(Canvas canvas) {
        drawables = new ArrayList<>();
        invalidate();
        draw(canvas);
    }

    public void addStar(Star star) {
        drawables.add(star);
    }

    private void setupPaint() {
        drawPaint = new Paint();
        drawPaint.setColor(Color.WHITE);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(20);
        drawPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    @Override
    public void setMinimumWidth(int minWidth) {
        super.setMinimumWidth(minWidth);
    }

    @Override
    public void setMinimumHeight(int minHeight) {
        super.setMinimumHeight(minHeight);
    }

    public void setSize(int width, int height) {
        this.setLayoutParams(new ConstraintLayout.LayoutParams(width, height));
    }

    public void setColor(int color) {
        drawPaint.setColor(color);
    }

    public void drawCircle(float x, float y, float radius) {
        canvas.drawCircle(x, y, radius, drawPaint);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.canvas = canvas;

        for (Drawable l : drawables) {
            drawPaint.setColor(l.getColor());

            l.draw(canvas, drawPaint);
        }
    }
}
