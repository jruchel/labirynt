package jruchel.labyrinth.drawing;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

public class Line implements Drawable {

    private Point a;
    private Point b;
    private int color;

    public Line(Point a, Point b, int color) {
        this.a = a;
        this.b = b;
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public Line(Point a, Point b) {
        this.a = a;
        this.b = b;
        this.color = Color.WHITE;
    }

    @Override
    public void draw(Canvas canvas, Paint drawPaint) {
        Path path = new Path();
        float ax = (float) a.x;
        float ay = (float) a.y;
        float bx = (float) b.x;
        float by = (float) b.y;
        path.moveTo(ax, ay);
        path.lineTo(bx, by);
        path.close();
        canvas.drawPath(path, drawPaint);
    }

}
