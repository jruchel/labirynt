package jruchel.labyrinth.drawing;

import android.graphics.Canvas;
import android.graphics.Paint;

public interface Drawable {

    void draw(Canvas canvas, Paint drawPaint);

    int getColor();

}
