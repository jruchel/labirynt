package jruchel.labyrinth.maze;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;


public class MazeSolver implements Runnable {

    private int pos;
    private Maze maze;
    private Stack<Tile> path;
    private List<Tile> visited;

    public MazeSolver(Maze maze) {
        this.maze = maze;
        this.path = new Stack<>();
        this.visited = new ArrayList<>();
        this.pos = 0;
    }

    @Override
    public void run() {
        path.add(maze.getTiles().get(pos));
        while (pos < maze.getSize() * maze.getSize() - 1) {
            moveToAdjacent();
        }
    }

    private void moveToAdjacent() {
        List<Tile> open = getNext();

        if (open.size() > 0) {
            Random random = new Random();
            int choice = Math.abs(random.nextInt()) % open.size();
            Tile t = open.get(choice);
            pos = t.index;
            path.push(t);
            visited.add(t);
        } else {
            path.pop();
            pos = path.peek().index;
            moveToAdjacent();
        }

    }

    private List<Tile> getNext() {
        List<Tile> result = filterVisited();
        return result;
    }

    private List<Tile> filterVisited() {
        List<Tile> open = getOpen();
        for (int i = open.size() - 1; i >= 0; i--) {
            if (visited.contains(open.get(i))) {
                open.remove(i);
            }
        }
        return open;
    }

    private List<Tile> getOpen() {
        List<Tile> tiles = maze.getTiles();
        List<Tile> result = new ArrayList<>();

        if (tiles.get(pos).getWalls()[0].isOpen()) {
            result.add(tiles.get(pos - maze.getSize()));
        }
        if (tiles.get(pos).getWalls()[1].isOpen()) {
            result.add(tiles.get(pos - 1));
        }
        if (tiles.get(pos).getWalls()[2].isOpen()) {
            result.add(tiles.get(pos + 1));
        }
        if (tiles.get(pos).getWalls()[3].isOpen()) {
            result.add(tiles.get(pos + maze.getSize()));
        }
        return result;
    }

    public int getMoves() {
        return path.size() - 1;
    }

    public Stack<Tile> getPath() {
        return path;
    }
}
