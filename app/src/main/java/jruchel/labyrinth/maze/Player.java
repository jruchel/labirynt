package jruchel.labyrinth.maze;

public class Player<T> {
    private T texture;
    private int position;

    public Player(T texture, int position) {
        this.texture = texture;
        this.position = position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setTexture(T texture) {
        this.texture = texture;
    }

    public int getPosition() {
        return position;
    }

    public T getTexture() {
        return texture;
    }
}
