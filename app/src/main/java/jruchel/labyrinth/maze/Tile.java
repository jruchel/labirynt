package jruchel.labyrinth.maze;


public class Tile {

    int index;
    private String content;
    private TileDisplay tileDisplay;
    private Wall walls[];

    public Tile(int index) {
        this.content = " ";
        this.index = index;
        createWalls();
    }

    private void createWalls() {
        this.walls = new Wall[4];
        walls[0] = new Wall(Direction.NORTH);
        walls[1] = new Wall(Direction.WEST);
        walls[2] = new Wall(Direction.EAST);
        walls[3] = new Wall(Direction.SOUTH);
    }

    public void display() {
        tileDisplay.display(this, index);
    }

    public void setTileDisplay(TileDisplay tileDisplay, int index) {
        this.tileDisplay = tileDisplay;
        this.index = index;
    }

    public Wall[] getWalls() {
        return walls;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public enum Direction {
        NORTH("N"), SOUTH("S"), WEST("W"), EAST("E");

        String symbol;

        Direction(String s) {
            this.symbol = s;
        }

    }

    public class Wall {
        private Direction direction;
        private boolean open;

        public Wall(Direction direction) {
            this.direction = direction;
            this.open = false;
        }

        public boolean isOpen() {
            return open;
        }

        public void setOpen(boolean open) {
            this.open = open;
        }

        public Direction getDirection() {
            return direction;
        }

        public void setDirection(Direction direction) {
            this.direction = direction;
        }
    }
}
