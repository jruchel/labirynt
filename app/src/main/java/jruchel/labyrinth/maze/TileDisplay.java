package jruchel.labyrinth.maze;



public interface TileDisplay {
    default void display(Tile tile, int i) {
        displayTop(tile.getWalls()[0], i);
        displayLeft(tile.getWalls()[1], i);
        displayContent(tile.getContent(), i);
        displayRight(tile.getWalls()[2], i);
        displayBottom(tile.getWalls()[3], i);

    }

    void displayTop(Tile.Wall top, int i);

    void displayBottom(Tile.Wall bot, int i);

    void displayLeft(Tile.Wall left, int i);

    void displayRight(Tile.Wall right, int i);

    void displayContent(String content, int i);
}
