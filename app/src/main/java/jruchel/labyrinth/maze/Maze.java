package jruchel.labyrinth.maze;


import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

public class Maze {

    private int moves = 0;
    private int size;
    private List<Tile> tiles;
    private Player player;
    private Runnable onGameOverListener;

    private Difficulty difficulty;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Maze(Difficulty difficulty) {
        this.difficulty = difficulty;
        this.size = difficulty.getSize();
        this.player = new Player("X", 0);
        tiles = new ArrayList<>();
        for (int i = 0; i < size * size; i++) {
            tiles.add(new Tile(i));
        }
        tiles.get(0).setContent((String) player.getTexture());

        generateMaze();
    }

    public List<Tile> getAdjacentTiles(int i) {
        return getAdjacentTiles(tiles.get(i));
    }

    public List<Tile> getAdjacentTiles(Tile t) {
        List<Tile> adjecent = new ArrayList<>();
        int i = t.index;
        try {
            if (i % size == size - 1) {
                throw new IndexOutOfBoundsException();
            }
            Tile right = tiles.get(i + 1);
            adjecent.add(right);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            if (i % size == 0) {
                throw new IndexOutOfBoundsException();
            }
            Tile left = tiles.get(i - 1);
            adjecent.add(left);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile bot = tiles.get(i + size);
            adjecent.add(bot);
        } catch (IndexOutOfBoundsException ignored) {
        }
        try {
            Tile top = tiles.get(i - size);
            adjecent.add(top);
        } catch (IndexOutOfBoundsException ignored) {
        }

        return adjecent;

    }

    public void display() {
        for (Tile t : tiles) {
            t.display();
        }
    }

    public List<Tile> getTiles() {
        return tiles;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void generateMaze() {
        MazeGenerator mazeGenerator = new MazeGenerator(this);
        mazeGenerator.generateMaze();
    }

    /**
     * Connects two tiles by opening their respective walls
     *
     * @param a first tile to connect
     * @param b second tile to connect
     * @return whether a connection was possible
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean connectTiles(Tile a, Tile b) {
        int first = a.index;
        int second = b.index;

        if (first >= second) {
            int aux = second;
            second = first;
            first = aux;
        }

        int distance = Math.abs(a.index - b.index);
        if (distance == 1) {
            tiles.get(first).getWalls()[2].setOpen(true);
            tiles.get(second).getWalls()[1].setOpen(true);
            return true;
        }
        if (distance == size) {
            tiles.get(first).getWalls()[3].setOpen(true);
            tiles.get(second).getWalls()[0].setOpen(true);
            return true;
        }
        return false;
    }


    public boolean isOver() {
        return player.getPosition() == size * size - 1;
    }

    public void movePlayer(Tile.Direction direction) {
        int tempPos = player.getPosition();
        switch (direction) {
            case NORTH:
                if (isLegal(direction)) {
                    player.setPosition(player.getPosition() - size);
                    moves++;
                    break;
                }
            case SOUTH:
                if (isLegal(direction)) {
                    player.setPosition(player.getPosition() + size);
                    moves++;
                    break;
                }
                break;
            case EAST:
                if (isLegal(direction)) {
                    player.setPosition(player.getPosition() + 1);
                    moves++;
                    break;
                }
                break;
            case WEST:
                if (isLegal(direction)) {
                    player.setPosition(player.getPosition() - 1);
                    moves++;
                    break;
                }
        }
        tiles.get(tempPos).setContent(" ");
        tiles.get(player.getPosition()).setContent((String) player.getTexture());

        if (isOver()) {
            onGameOverListener.run();
        }
    }

    public void setOnGameOverListener(Runnable onGameOverListener) {
        this.onGameOverListener = onGameOverListener;
    }

    private boolean isLegal(Tile.Direction direction) {
        switch (direction) {
            case NORTH:
                if (player.getPosition() / size != 0) {
                    return tiles.get(player.getPosition()).getWalls()[0].isOpen() && tiles.get(player.getPosition() - size).getWalls()[3].isOpen();
                }
                return false;
            case SOUTH:
                if (player.getPosition() / size != (size - 1)) {
                    return tiles.get(player.getPosition()).getWalls()[3].isOpen() && tiles.get(player.getPosition() + size).getWalls()[0].isOpen();
                }
                return false;
            case EAST:
                if (player.getPosition() % size != (size - 1)) {
                    return tiles.get(player.getPosition()).getWalls()[2].isOpen() && tiles.get(player.getPosition() + 1).getWalls()[1].isOpen();
                }
                return false;
            case WEST:
                if (player.getPosition() % size != 0) {
                    return tiles.get(player.getPosition()).getWalls()[1].isOpen() && tiles.get(player.getPosition() - 1).getWalls()[2].isOpen();
                }
                return false;
            default:
                return false;
        }
    }

    public float getScore() {
        float score = (100 - (((float) moves - getMinMoves()) / getMinMoves() * 100));
        return score < 0 ? 0 : score;
    }

    private int getMinMoves() {
        MazeSolver mazeSolver = new MazeSolver(this);
        mazeSolver.run();
        return mazeSolver.getMoves();
    }

    public int getSize() {
        return size;
    }

    public void setTilesDisplay(TileDisplay display) {
        for (int i = 0; i < size * size; i++) {
            tiles.get(i).setTileDisplay(display, i);
        }
    }

    public int getMoves() {
        return moves;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void reset() {
        this.player = new Player("X", 0);
        tiles = new ArrayList<>();
        moves = 0;
        for (int i = 0; i < size * size; i++) {
            tiles.add(new Tile(i));
        }
        tiles.get(0).setContent((String) player.getTexture());

        generateMaze();
    }

    public Player getPlayer() {
        return player;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
        this.size = difficulty.getSize();
    }

    public enum Difficulty {
        EASY, MEDIUM, HARD;

        public int getSize() {
            switch (this) {
                case EASY:
                    return 7;
                case MEDIUM:
                    return 11;
                case HARD:
                    return 14;
                default:
                    return 10;
            }
        }
    }
}
