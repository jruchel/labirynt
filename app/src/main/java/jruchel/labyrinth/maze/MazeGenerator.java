package jruchel.labyrinth.maze;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class MazeGenerator {

    private Stack<Tile> tileStack;
    private List<Tile> visited;
    private Maze maze;
    private Tile current;

    public MazeGenerator(Maze maze) {
        this.maze = maze;
        this.tileStack = new Stack<>();
        this.visited = new ArrayList<>();
        current = maze.getTiles().get(0);
        tileStack.push(current);
        visited.add(current);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void generateMaze() {
        while (visited.size() != maze.getTiles().size()) {
            Tile next = getNext(current);
            maze.connectTiles(current, next);
            tileStack.push(next);
            current = next;
            visited.add(current);
        }

    }

    private Tile getNext(Tile tile) {
        Random random = new Random();
        List<Tile> adjacent = maze.getAdjacentTiles(tile);
        Tile last = maze.getTiles().get(maze.getSize() * maze.getSize() - 1);

        //Usuwanie wszystkich odwiedzonych kafelkow z listy
        for (int i = adjacent.size() - 1; i >= 0; i--) {
            if (visited.contains(adjacent.get(i))) {
                adjacent.remove(adjacent.get(i));
            }
        }
        //Jesli po usunieciu odwiedzonych lista jest pusta, ma cofnac sie o jedno miejsce w stosie i sprawdzic jeszcze raz
        if (adjacent.isEmpty()) {
            tileStack.pop();
            current = tileStack.peek();
            return getNext(tileStack.peek());
        }
        //Nie moze wybrac kafelka konca gry, chyba ze jest to ostatni mozliwy kafelek
        if (adjacent.size() > 1 && adjacent.contains(last)) {
            adjacent.remove(last);
        }

        return adjacent.get(Math.abs(random.nextInt()) % adjacent.size());
    }

}

