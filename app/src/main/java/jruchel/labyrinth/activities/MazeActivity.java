package jruchel.labyrinth.activities;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import jruchel.labyrinth.R;
import jruchel.labyrinth.drawing.CanvasDisplay;
import jruchel.labyrinth.drawing.Line;
import jruchel.labyrinth.drawing.Point;
import jruchel.labyrinth.drawing.Star;
import jruchel.labyrinth.maze.Maze;
import jruchel.labyrinth.maze.Tile;
import jruchel.labyrinth.maze.TileDisplay;

public class MazeActivity extends AppCompatActivity {

    private Maze maze;
    private int size;
    private double tileSize = 100;
    private double xOffset;


    private TileDisplay tileDisplay;

    private CanvasDisplay canvas;
    private Canvas can;

    private Button up;
    private Button down;
    private Button left;
    private Button right;

    private TextView movesView;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        canvas = findViewById(R.id.canvas);
        can = new Canvas();
        canvas.draw(can);

        maze = new Maze(getSetDifficulty());
        size = getSetDifficulty().getSize();
        setxOffset();

        maze.setOnGameOverListener(() -> createGameOverDialog().show());

        tileDisplay = new TileDisplay() {

            @Override
            public void display(Tile tile, int i) {
                displayTop(tile.getWalls()[0], i);
                displayLeft(tile.getWalls()[1], i);
                displayContent(tile.getContent(), i);
                displayRight(tile.getWalls()[2], i);
                displayBottom(tile.getWalls()[3], i);
            }

            @Override
            public void displayTop(Tile.Wall top, int i) {
                if (!maze.getTiles().get(i).getWalls()[0].isOpen()) {
                    double x = tileSize * (i % size);
                    x += xOffset;
                    double y = tileSize * ((i / size) + 1);
                    Point start = new Point(x, y);
                    Point end = new Point(x + tileSize, y);
                    canvas.addLine(new Line(start, end));
                }
            }

            @Override
            public void displayBottom(Tile.Wall bot, int i) {
                if (!maze.getTiles().get(i).getWalls()[3].isOpen()) {
                    double x = tileSize * (i % size);
                    x += xOffset;
                    double y = (tileSize * ((i / size) + 1));
                    Point start = new Point(x, y + tileSize);
                    Point end = new Point(x + tileSize, y + tileSize);
                    canvas.addLine(new Line(start, end));
                }
            }

            @Override
            public void displayLeft(Tile.Wall left, int i) {
                if (!maze.getTiles().get(i).getWalls()[1].isOpen()) {
                    double x = tileSize * (i % size);
                    x += xOffset;
                    double y = tileSize * ((i / size) + 1);
                    Point start = new Point(x, y);
                    Point end = new Point(x, y + tileSize);
                    canvas.addLine(new Line(start, end));
                }
            }

            @Override
            public void displayRight(Tile.Wall right, int i) {
                if (!maze.getTiles().get(i).getWalls()[2].isOpen()) {
                    double x = tileSize * (i % size);
                    x += xOffset;
                    double y = tileSize * ((i / size) + 1);
                    Point start = new Point(x + tileSize, y);
                    Point end = new Point(x + tileSize, y + tileSize);
                    canvas.addLine(new Line(start, end));
                }
            }

            @Override
            public void displayContent(String content, int i) {
                if (maze.getTiles().get(i).getContent() == "X") {
                    double x = tileSize * (i % size);
                    x += xOffset;
                    double y = tileSize * ((i / size) + 1);
                    Point start = new Point(x, y);
                    Point end = new Point(x + tileSize, y + tileSize);
                    canvas.addLine(new Line(start, end));
                    start = new Point(x, y + tileSize);
                    end = new Point(x + tileSize, y);
                    canvas.addLine(new Line(start, end));
                }
            }

        };

        maze.setTilesDisplay(tileDisplay);
        maze.display();
        initializeControls();
        initializeViews();
    }

    private double getBoardScreenWidth() {
        return size * tileSize;
    }

    private android.graphics.Point getScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        android.graphics.Point size = new android.graphics.Point();
        display.getSize(size);
        return size;
    }

    private Point getScreenCenter() {

        android.graphics.Point size = getScreenSize();

        return new Point(size.x / 2, size.y / 2);
    }

    private void setxOffset() {
        xOffset = (getScreenSize().x - getBoardScreenWidth()) / 2;
    }

    private void initializeViews() {
        movesView = findViewById(R.id.moves);
        movesView.setText(String.format(getString(R.string.moves), maze.getMoves()));
    }

    private void initializeControls() {
        up = findViewById(R.id.up);
        up.setOnClickListener(v -> {
            canvas.update(can);
            maze.movePlayer(Tile.Direction.NORTH);
            movesView.setText(String.format(getString(R.string.moves), maze.getMoves()));
            maze.display();
        });
        down = findViewById(R.id.down);
        down.setOnClickListener(v -> {
            canvas.update(can);
            maze.movePlayer(Tile.Direction.SOUTH);
            movesView.setText(String.format(getString(R.string.moves), maze.getMoves()));
            maze.display();
        });
        left = findViewById(R.id.left);
        left.setOnClickListener(v -> {
            canvas.update(can);
            maze.movePlayer(Tile.Direction.WEST);
            movesView.setText(String.format(getString(R.string.moves), maze.getMoves()));
            maze.display();
        });
        right = findViewById(R.id.right);
        right.setOnClickListener(v -> {
            canvas.update(can);
            maze.movePlayer(Tile.Direction.EAST);
            movesView.setText(String.format(getString(R.string.moves), maze.getMoves()));
            maze.display();
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private AlertDialog createGameOverDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(false);
        dialog.setTitle(getString(R.string.gameover));
        //dialog.setMessage(String.format("Maze Completed. Score: %.0f", maze.getScore()));
        ConstraintLayout constraintLayout = (ConstraintLayout) getLayoutInflater().inflate(R.layout.dialog_canvas, null, false);
        dialog.setView(constraintLayout);
        CanvasDisplay canvasDisplay = (CanvasDisplay) constraintLayout.getViewById(R.id.dialog_canvas);
        int color1 = Color.GRAY;
        int color2 = Color.GRAY;
        int color3 = Color.GRAY;
        if (maze.getScore() > 40) {
            color1 = Color.YELLOW;
        }
        if (maze.getScore() > 70) {
            color2 = Color.YELLOW;
        }
        if (maze.getScore() >= 100) {
            color3 = Color.YELLOW;
        }
        canvasDisplay.addStar(new Star(100, new Point(150, 250), 5, color1));
        canvasDisplay.addStar(new Star(100, new Point(400, 250), 5, color2));
        canvasDisplay.addStar(new Star(100, new Point(650, 250), 5, color3));



        dialog.setPositiveButton("Ok", (dialog1, which) -> {
            maze.reset();
            movesView.setText(String.format(getString(R.string.moves), maze.getMoves()));
            setxOffset();
            maze.setTilesDisplay(tileDisplay);
            canvas.update(can);
            maze.display();
        });

        return dialog.create();
    }

    private Maze.Difficulty getSetDifficulty() {
        switch (getIntent().getIntExtra("dif", 2)) {
            case 1:
                return Maze.Difficulty.EASY;
            case 2:
                return Maze.Difficulty.MEDIUM;
            case 3:
                return Maze.Difficulty.HARD;
            default:
                return Maze.Difficulty.MEDIUM;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MazeActivity.this, MenuActivity.class);
        startActivity(intent);
        finish();
    }
}
