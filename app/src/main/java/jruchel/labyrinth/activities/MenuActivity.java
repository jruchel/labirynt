package jruchel.labyrinth.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import jruchel.labyrinth.R;
import jruchel.labyrinth.drawing.CanvasDisplay;
import jruchel.labyrinth.drawing.Point;
import jruchel.labyrinth.drawing.Star;

public class MenuActivity extends AppCompatActivity {

    private Button easy;
    private Button medium;
    private Button hard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initViews();
    }

    private void initViews() {
        easy = findViewById(R.id.dif_easy);
        easy.setOnClickListener(v -> {
            Intent intent = new Intent(MenuActivity.this, MazeActivity.class);
            intent.putExtra("dif", 1);
            startNextActivity(intent);
        });
        medium = findViewById(R.id.dif_mid);
        medium.setOnClickListener(v -> {
            Intent intent = new Intent(MenuActivity.this, MazeActivity.class);
            intent.putExtra("dif", 2);
            startNextActivity(intent);
        });
        hard = findViewById(R.id.dif_high);
        hard.setOnClickListener(v -> {
            Intent intent = new Intent(MenuActivity.this, MazeActivity.class);
            intent.putExtra("dif", 3);
            startNextActivity(intent);
        });
    }

    private void startNextActivity(Intent intent) {
        startActivity(intent);
        finish();
    }

}
