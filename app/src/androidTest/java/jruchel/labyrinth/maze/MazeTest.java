package jruchel.labyrinth.maze;

import org.junit.Assert;
import org.junit.Test;

public class MazeTest {


    @Test
    public void getScore() {

        double expected = 147.27;

        int moves = 55;
        int minMoves = 54;
        float mult = 1.5f;
        double delta = 0.1f;
        float result = (100 - (((float) moves - minMoves) / minMoves) * 100) * mult;
        Assert.assertEquals(expected, result, delta);
    }

}